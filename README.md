# Технологии разработки программного обеспечения
### Лабораторная работа №1: создание микросервиса на Spring Boot с базой данных
Самохин Евгений Григорьевич МАС2131

Цель лабораторной работы: знакомство с проектированием многослойной архитектуры Web-API (веб-приложений, микро-сервисов).

Для начала работы
1. Склонируйте репозиторий в папку сборки (git clone адресс проекта)
2. Зайдите в склонированную директорию и выполните команду mvn package
3. Для сборки docker образа выполните команду docker build . -t <имя>:<версия>
4. Для запуска docker контейнера с мапингом портов выплнить docker run -p 8080:8080 <имя>:<версия>
5. Обращение к ендпоинтам: 
http://localhost:8080/api/v1/products/2, 
http://localhost:8080/api/v1/products/1,
http://localhost:8080/api/v1/products,
http://localhost:8080/api/v1/products/2
6. Ендпоинт возварщающий hostname: 
http://localhost:8080/api/v1/status.


### Лабораторная работа №3: CI/CD и деплой приложения в Heroku
Цель лабораторной работы: знакомство с CI/CD и его реализация на примере Travis CI и Heroku.  

https://simpleapi-samohin.herokuapp.com/api/v1/status

https://simpleapi-samohin.herokuapp.com/api/v1/products

[![Build Status](https://app.travis-ci.com/EvgeniySam/javaapi.svg?branch=main)](https://app.travis-ci.com/EvgeniySam/javaapi)
