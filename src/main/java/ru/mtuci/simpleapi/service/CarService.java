package ru.mtuci.simpleapi.service;

import ru.mtuci.simpleapi.model.Car;

import java.util.List;

public interface CarService {
    Car get(Long id);
    List<Car> getAll();
    Car save(Car product);
    void delete(Long id);
}
