package ru.mtuci.simpleapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mtuci.simpleapi.dao.CarRepository;
import ru.mtuci.simpleapi.model.Car;

import java.util.List;

@Service
public class CarServiceImpl implements CarService{
    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository){
        this.carRepository = carRepository;
    }

    @Override
    public Car get(Long id) {
        return carRepository.findById(id).orElse(null);
    }

    @Override
    public List<Car> getAll() {
        return carRepository.findAll();
    }

    @Override
    public Car save(Car product) {
        return carRepository.save(product);
    }

    @Override
    public void delete(Long id) {
        carRepository.delete(id);
    }
}
