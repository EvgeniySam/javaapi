package ru.mtuci.simpleapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.mtuci.simpleapi.model.Car;
import ru.mtuci.simpleapi.service.CarService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = CarController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class CarController {
    public static final String REST_URL ="/api/v1/products";

    private CarService carService;
    @Autowired
    public CarController(CarService carService){
        this.carService = carService;
    }

    @GetMapping
    public List<Car> getAll(){
        log.info("get All");
        return carService.getAll();
    }

    @GetMapping(value = "/{id}")
    public Car get(@PathVariable("id") Long id){
        log.info("get " + id);
        return carService.get(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Car save(@RequestBody Car product){
        log.info("save " + product);
        return carService.save(product);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id){
        log.info("Delete " + id);
        carService.delete(id);
    }


}
