package ru.mtuci.simpleapi.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Car extends AbstractBaseEntity{
    @NotBlank
    private String brand;
    @NotBlank
    private String model;
    @NotNull
    private String drive;
    @NotNull
    private Integer price;
    @NotNull
    private String color;
}
