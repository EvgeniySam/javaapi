package ru.mtuci.simpleapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.mtuci.simpleapi.model.Car;
import ru.mtuci.simpleapi.dao.CarRepository;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private final CarRepository carRepository;

    @Autowired
    public CommandLineAppStartupRunner(CarRepository carRepository){
        this.carRepository = carRepository;
    }

    @Override
    public void run(String...args) throws Exception{
        //System.out.println(productRepository.findById(1L).get());
    }

}
