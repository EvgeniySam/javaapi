DROP TABLE IF EXISTS car CASCADE;
DROP SEQUENCE IF EXISTS global_seq;
CREATE SEQUENCE global_seq START WITH 1;

CREATE TABLE car(
    id          INTEGER PRIMARY KEY DEFAULT  nextval('global_seq'),
    brand       VARCHAR         NOT NULL,
    model       VARCHAR         NOT NULL,
    drive       VARCHAR         NOT NULL,
    price       INTEGER         NOT NULL,
    color       VARCHAR         NOT NULL
);